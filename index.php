<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Uikit Studies</title>
    <link rel="stylesheet" href="css/uikit.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="js/uikit.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
  </head>
<body>
<style media="screen">

</style>
<!-- This is the off-canvas sidebar -->
<div id="my-id" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">...</div>
</div>

<nav class="uk-navbar">
  <ul class="uk-navbar-nav">
    <li><a href="#my-id" data-uk-offcanvas>open</a></li>
  </ul>

  <ul class="uk-navbar-nav uk-navbar-flip" id="headertoggle"> <!-- Here the word "nav" is responsible by organize the itens side by side -->

    <li class="uk-active"><a href="">Active</a></li>
    <li><a href="">Item</a></li>
    <li><a href="#">Item</a></li>
    <li class="" data-uk-dropdown>
    <a href="">Dropdown</a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul>
          <li>Test Child Dropdown</li>
          <li>Test Child Dropdown 2</li>
        </ul>
      </div>
    </li>

  </ul>
</nav>

<div class="hero">
  <h2>But I must explain to you how all this mistaken</h2>
</div>

</div>

<div class="uk-container uk-container-center">
  <div class="uk-container">
    <ul class="uk-grid uk-grid-small uk-grid-width-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-6" data-uk-grid-margin>
      <li>
        <div class="uk-panel uk-panel-box">
          <h2>Teste seção</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien.</p>
        </div>
      </li>
      <li>
        <div class="uk-panel uk-panel-box">
          <h2>Teste seção</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien.</p>
        </div>
      </li>
      <li>
        <div class="uk-panel uk-panel-box">
          <h2>Teste seção</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien.</p>
        </div>
      </li>
      <li>
        <div class="uk-panel uk-panel-box">
          <h2>Teste seção</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien.</p>
        </div>
      </li>
      <li>
        <div class="uk-panel uk-panel-box">
          <h2>Teste seção</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien.</p>
        </div>
      </li>
      <li>
        <div class="uk-panel uk-panel-box">
          <h2>Teste seção</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien.</p>
        </div>
      </li>
    </ul>
  </div>
</div>



<?php
$post = require 'post.php';

$i = 1;

while ($i <= 10):
    echo $i;
    $i++;
endwhile;

?>

</body>
</html>
