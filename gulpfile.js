var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    plumber = require('gulp-plumber');


gulp.task('styles', function(){
  gulp.src('build/sass/*.sass')
  .pipe(plumber())
  .pipe(sass())
  .pipe(gulp.dest('css'));

});

gulp.task('minifyjs', function(){
  gulp.src('build/js/*.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest('build/js/'));
});

gulp.task('build-img', function(){
  gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('src/img'));
});

gulp.task('watch', function(){
  gulp.watch('build/sass/style.sass', ['styles'])
  gulp.watch('build/js/*.js', ['minifyjs']);
});

gulp.task('default', ['styles', 'minifyjs','watch']);
